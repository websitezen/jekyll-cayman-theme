---
layout: default
title:  "UptimeZen"
date:   2019-02-06 17:50:00
categories: main
---
**Hello World!**

It has been a few months since we put together an amazing team and started coding a tool which we would use to ease our everyday life as developers. As days went by, we came to realize that the tool we built for solving our problems, could be of benefit to many developers out there in the World having similar struggles.

Hence began our journey building Uptimezen but before making the official announcement we wanted to talk a little about why we are building this!

First of, we are a team of people who has long spread experience in developing and maintaining SaaS Apps from a range of  top-notch startups building SaaS apps to Fortune 500 Enterprises delivering huge IT projects. We have first hand experience of what it is to be blamed for downtimes, sticking to strict SLAs or just simply losing revenue due to outages.

As a developer, webmaster or a business user, you spent time and money to get your website or service going. We are well aware that, it’s easier said than done. You spent hours thinking, selecting tools, prototyping, developing or hiring someone to do it for yourself - you even spent good hours to think about the domain name to purchase or how to name the app. And after a good enough time period, you got your results: a perfect website/service/app up and running! You tested it days before launch and once the time is right you were proud with the result, collecting taps on the shoulder!

Then what happens? Can you just move onwards with your other tasks and assume it is sitting there, up and running perfectly? You know, you just can’t!

Number of complaints would be tenfold of the “Kudos!” you’ve got if something bad happens to your website/app, right? We all experienced it. Another thing we know for sure is, it is way more fun building it then working for keeping it up!

Say it be DevOps,  Development Teams, Site Reliability Engineers, Webmasters, Wordpress Builders, Wix Website Owners, Joomla Users, Frontend Engineers, Kubernetes Users, System Engineers or Business Owners, MSPs, E-commerce Website Owners, Blog Masters.. List can grow further than the hold of this blog page ever could.

We want to give you the clear state of mind when it comes to the status of your website/app -because we definitely need the same. Hence comes the idea behind making Uptimezen a public and free tool at the disposal of every single website/app in the World!

And of course the story doesn’t begin nor end here. We are well aware of the alternative “uptime check” and “monitoring” tools out there in the market. To be honest, they are all doing a great job. Pingdom, Uptrends, Uptimerobot, Statuscake they are all great tools helping the people out there solving similar problems. And being even more honest, we even paid to use their services during our long history dealing with such struggles. But still, we had to build our own service so that we can have following :

1. No Cost!
2. 1 Second Setup - Zero Configuration!
3. 1 Minute Checks!
4. 10 Locations!
5. No False Positives!
6. Root Cause in Mailbox without leaving our seat if something goes wrong!

This defined our initial task list and upon conclusion, literally gave us “Zen State of Mind” when it came to our website/apps - which were more than 200 that we had to deal with everyday!

Pretty soon, we will open the same to public so that the good people of Earth can get to achieve the same level of freedom and a clear state of mind -namely the Zen State- when it comes to their website!

This will merely be the beginning and in months time we will keep adding up and give you an AI backed Zen Master who takes care of everything going on with your website while you are out there having your day!

Keep watching
